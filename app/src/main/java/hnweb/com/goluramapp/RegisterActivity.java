package hnweb.com.goluramapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import hnweb.com.goluramapp.utility.Validations;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    EditText nameET, emailET, passET, repassET;
    String name, email, pass, repass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();

    }

    public void init() {
        nameET = (EditText) findViewById(R.id.fullnameET);
        emailET = (EditText) findViewById(R.id.emailET);
        passET = (EditText) findViewById(R.id.passET);
        repassET = (EditText) findViewById(R.id.conpassET);

    }

    public void getData() {
        name = nameET.getText().toString().trim();
        email = emailET.getText().toString().trim();
        pass = passET.getText().toString().trim();
        repass = repassET.getText().toString().trim();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.registerBTN:
                checkValid();
                break;
        }
    }

    public void checkValid() {
        getData();
        if (Validations.strslength(name) && Validations.strslength(email) && Validations.strslength(pass) && Validations.strslength(repass)) {
            if (Validations.strslength(name)) {
                if (Validations.emailCheck(email)) {
                    if (Validations.strslength(pass)) {
                        if (Validations.confirmPass(pass, repass)) {
                            Toast.makeText(this, "Coming soon ...", Toast.LENGTH_SHORT).show();
                        } else {
                            repassET.setError("Please enter correct password.");
                            repassET.requestFocus();
                        }
                    } else {
                        passET.setError("Please enter password.");
                        passET.requestFocus();
                    }
                } else {
                    emailET.setError("Please enter valid email id.");
                    emailET.requestFocus();
                }
            } else {
                nameET.setError("Please enter full name.");
                nameET.requestFocus();
            }
        } else {
            nameET.setError("Please enter full name.");
            nameET.requestFocus();
            emailET.setError("Please enter valid email id.");
            passET.setError("Please enter password.");
            repassET.setError("Please enter correct password.");
        }


    }
}
