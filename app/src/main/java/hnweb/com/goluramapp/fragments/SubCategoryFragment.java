package hnweb.com.goluramapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import hnweb.com.goluramapp.CategoriesActivity;
import hnweb.com.goluramapp.R;
import hnweb.com.goluramapp.utility.AppConstants;

/**
 * Created by neha on 2/6/2017.
 */

public class SubCategoryFragment extends Fragment {
    ListView listView;
    String[] subCategoryList = new String[]{"Jhoomar(10)", "Led Lighting(8)", "LED Lights(17)"};

    public SubCategoryFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.category_fragment_layout, container, false);
        listView = (ListView) myFragmentView.findViewById(R.id.listView);
        ((CategoriesActivity) getContext()).getSupportActionBar().setTitle(AppConstants.CATHEAD);
        listView.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.text_view, subCategoryList));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AppConstants.SUBCATHEAD = subCategoryList[i].toString().trim();
                Log.e("ITEM", subCategoryList[i].toString());
                ProductsFragment pf = new ProductsFragment();
                ((CategoriesActivity) getContext()).replaceFragment(pf);
            }
        });
        return myFragmentView;
    }
}
