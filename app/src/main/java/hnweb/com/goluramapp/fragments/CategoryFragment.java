package hnweb.com.goluramapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import hnweb.com.goluramapp.CategoriesActivity;
import hnweb.com.goluramapp.R;
import hnweb.com.goluramapp.utility.AppConstants;

/**
 * Created by neha on 2/6/2017.
 */

public class CategoryFragment extends Fragment {
    String[] categoryList = new String[]{"BAGS", "ELECTRONIC EQUIPMENTS", "HAVELLS", "WATER FOUNTAIN", "GIFTS", "LIGHTING", "DIGITAL PRODUCTS", "APPAREL"};
    ListView listView;


    public CategoryFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.category_fragment_layout, container, false);
        listView = (ListView) myFragmentView.findViewById(R.id.listView);
        ((CategoriesActivity) getContext()).getSupportActionBar().setTitle("Category");
        listView.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.text_view, categoryList));


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("ITEM", categoryList[i].toString());
                AppConstants.CATHEAD = categoryList[i].toString().trim();
                if (categoryList[i].toString().equalsIgnoreCase("LIGHTING")) {
                    SubCategoryFragment scf = new SubCategoryFragment();
                    ((CategoriesActivity) getContext()).replaceFragment(scf);
                }


            }
        });

//        myFragmentView.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View view, int i, KeyEvent keyEvent) {
//                if (i == KeyEvent.KEYCODE_BACK) {
//                    Toast.makeText(getActivity(), "Back", Toast.LENGTH_SHORT).show();
//                    return true;
//                }
//                return false;
//            }
//        });
        return myFragmentView;
    }


}
