package hnweb.com.goluramapp.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import hnweb.com.goluramapp.CategoriesActivity;
import hnweb.com.goluramapp.R;
import hnweb.com.goluramapp.adapter.MyCartAdapter;
import hnweb.com.goluramapp.utility.AppConstants;

/**
 * Created by neha on 2/7/2017.
 */

public class MyCartFragment extends Fragment implements View.OnClickListener {
    public RecyclerView cartRV;
    TextView totalpriceTV, checkoutTV;
    RelativeLayout emptyCartTV;
    int total = 0;
    MyCartAdapter pla;

    public MyCartFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.my_cart_fragment, container, false);
        ((CategoriesActivity) getContext()).getSupportActionBar().setTitle("My Cart");
        totalpriceTV = (TextView) myFragmentView.findViewById(R.id.totalpriceTV);
        checkoutTV = (TextView) myFragmentView.findViewById(R.id.checkoutTV);
        emptyCartTV = (RelativeLayout) myFragmentView.findViewById(R.id.emptyCartTV);

        emptyCartTV.setOnClickListener(this);
        checkoutTV.setOnClickListener(this);
        for (int i = 0; i < AppConstants.pPriceList.size(); i++) {
            total = total + (Integer.parseInt(AppConstants.pPriceList.get(i).toString().trim()) * Integer.parseInt(AppConstants.pQuantityList.get(i).toString().trim()));
        }
        AppConstants.TOTAL = total;
        totalpriceTV.setText(String.valueOf(total));

        setRecyclerView(myFragmentView);

        return myFragmentView;
    }


    public void setRecyclerView(View v) {
        cartRV = (RecyclerView) v.findViewById(R.id.cartRV);
        cartRV.setLayoutManager(new LinearLayoutManager(getActivity()));

        pla = new MyCartAdapter(getActivity(), AppConstants.pNameList, AppConstants.pImgList, AppConstants.pQuantityList, AppConstants.pPriceList, totalpriceTV);

        cartRV.setAdapter(pla);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.checkoutTV:
                CheckOutFragment cof = new CheckOutFragment();
                ((CategoriesActivity) getContext()).replaceFragment(cof);
                break;
            case R.id.emptyCartTV:
                if (AppConstants.pNameList.size()>0){
                    emptyCart(getActivity());
                }else {
                    Toast.makeText(getActivity(), "Already cart is empty", Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }

    public void emptyCart(final Activity context) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
//        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setCancelable(false);
        builder.setMessage("Are you sure you want to make cart empty.");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                AppConstants.pPriceList.clear();
                AppConstants.pQuantityList.clear();
                AppConstants.pImgList.clear();
                AppConstants.pNameList.clear();
                total = 0;

                pla.notifyDataSetChanged();
//                cartRV.setAdapter(pla);
                totalpriceTV.setText(String.valueOf(total));
                TextView itemCountTV = (TextView) getActivity().findViewById(R.id.itemCountTV);
                if (AppConstants.pNameList.size() < 10) {

                    itemCountTV.setText("0" + AppConstants.pNameList.size());
                } else {
                    itemCountTV.setText(AppConstants.pNameList.size() + "");
                }
                Toast.makeText(getActivity(), "Now your cart is empty", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();


    }

}
