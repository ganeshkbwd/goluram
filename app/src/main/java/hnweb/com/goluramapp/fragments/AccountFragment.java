package hnweb.com.goluramapp.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import hnweb.com.goluramapp.CategoriesActivity;
import hnweb.com.goluramapp.R;
import hnweb.com.goluramapp.utility.Validations;

/**
 * Created by neha on 2/7/2017.
 */

public class AccountFragment extends Fragment implements View.OnClickListener {
    TextView passwordTV, emailTV, confirmTV;
    EditText emailET, oldPassET, newPassET, confirmPassET;
    LinearLayout changePassLL;

    public AccountFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.account_fragment, container, false);
        ((CategoriesActivity) getContext()).getSupportActionBar().setTitle("Account Details");

        passwordTV = (TextView) myFragmentView.findViewById(R.id.passwordTV);
        changePassLL = (LinearLayout) myFragmentView.findViewById(R.id.changePassLL);
        emailTV = (TextView) myFragmentView.findViewById(R.id.emailTV);
        emailET = (EditText) myFragmentView.findViewById(R.id.emailET);
        oldPassET = (EditText) myFragmentView.findViewById(R.id.oldPassET);
        newPassET = (EditText) myFragmentView.findViewById(R.id.newPassET);
        confirmPassET = (EditText) myFragmentView.findViewById(R.id.confirmPassET);
        confirmTV = (TextView) myFragmentView.findViewById(R.id.confirmTV);

        confirmTV.setOnClickListener(this);
        emailTV.setOnClickListener(this);
        passwordTV.setOnClickListener(this);

        return myFragmentView;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.passwordTV:
                if (changePassLL.getVisibility() == View.VISIBLE) {
                    passwordTV.setTextColor(Color.WHITE);
                    passwordTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.password_icon_white, 0, R.drawable.down_arrow, 0);
                    changePassLL.setVisibility(View.GONE);
                } else {
                    passwordTV.setTextColor(Color.RED);
                    passwordTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.password_icon, 0, R.drawable.down_arrow, 0);

                    changePassLL.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.emailTV:
                if (emailET.getVisibility() == View.VISIBLE) {
                    emailTV.setTextColor(Color.WHITE);
                    emailTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mail, 0, R.drawable.down_arrow, 0);
                    emailET.setVisibility(View.GONE);
                } else {
                    emailTV.setTextColor(Color.RED);
                    emailTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mail_red, 0, R.drawable.down_arrow, 0);
                    emailET.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.confirmTV:
                checkValid();
                break;
        }
    }

    public void checkValid() {
        if (Validations.strslength(oldPassET.getText().toString().trim()) == false &&
                Validations.strslength(newPassET.getText().toString().trim()) == false
                && Validations.strslength(confirmPassET.getText().toString().trim())) {

            oldPassET.setError("Please enter old password.");
            oldPassET.requestFocus();
            newPassET.setError("Please enter new password.");
            confirmPassET.setError("Please enter confirm password.");
        } else if (Validations.strslength(oldPassET.getText().toString().trim()) == false) {
            oldPassET.setError("Please enter old password.");
            oldPassET.requestFocus();
        } else if (Validations.strslength(newPassET.getText().toString().trim()) == false) {
            newPassET.setError("Please enter new password.");
            newPassET.requestFocus();
        } else if (Validations.confirmPass(newPassET.getText().toString().trim(),
                confirmPassET.getText().toString().trim())) {
            confirmPassET.setError("Please enter confirm password.");
            confirmPassET.requestFocus();
        } else {
            Toast.makeText(getActivity(), "Coming soon...", Toast.LENGTH_SHORT).show();
        }
    }
}
