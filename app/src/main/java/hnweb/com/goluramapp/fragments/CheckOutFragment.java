package hnweb.com.goluramapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import hnweb.com.goluramapp.CategoriesActivity;
import hnweb.com.goluramapp.R;
import hnweb.com.goluramapp.adapter.CheckOutAdapter;
import hnweb.com.goluramapp.utility.AppConstants;

/**
 * Created by neha on 2/8/2017.
 */

public class CheckOutFragment extends Fragment implements View.OnClickListener {
    RecyclerView pListRV;
    TextView confirmTV, totalpriceTV, shippingPriceTV;
    int shippingcharge = 0;

    public CheckOutFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.checkout_fragment_layout, container, false);
        ((CategoriesActivity) getContext()).getSupportActionBar().setTitle("Checkout");
        init(myFragmentView);
        setRecyclerView(myFragmentView);
        return myFragmentView;
    }

    public void init(View v) {
        totalpriceTV = (TextView) v.findViewById(R.id.totalpriceTV);
        shippingPriceTV = (TextView) v.findViewById(R.id.shippingPriceTV);
        confirmTV = (TextView) v.findViewById(R.id.confirmTV);
        confirmTV.setOnClickListener(this);
        setData();
    }

    public void setData() {
        totalpriceTV.setText("Total: " + (shippingcharge + AppConstants.TOTAL));

    }

    public void setRecyclerView(View v) {
        pListRV = (RecyclerView) v.findViewById(R.id.pListRV);
        pListRV.setLayoutManager(new LinearLayoutManager(getActivity()));

        CheckOutAdapter pla = new CheckOutAdapter(getActivity(), AppConstants.pNameList, AppConstants.pQuantityList, AppConstants.pPriceList);

        pListRV.setAdapter(pla);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirmTV:
                AppConstants.pNameList.clear();
                AppConstants.pPriceList.clear();
                AppConstants.pQuantityList.clear();
                AppConstants.pImgList.clear();
                while(getActivity().getSupportFragmentManager().popBackStackImmediate()){}
                CategoryFragment cf = new CategoryFragment();
                ((CategoriesActivity) getActivity()).replaceFragment(cf);
                Toast.makeText(getActivity(), "Order confirmed successfully...", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
