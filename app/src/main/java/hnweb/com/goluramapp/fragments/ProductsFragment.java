package hnweb.com.goluramapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hnweb.com.goluramapp.CategoriesActivity;
import hnweb.com.goluramapp.R;
import hnweb.com.goluramapp.adapter.ProductListAdapter;
import hnweb.com.goluramapp.utility.AppConstants;

/**
 * Created by neha on 2/6/2017.
 */

public class ProductsFragment extends Fragment {
    RecyclerView productRV;
    String[] productNamesList = new String[]{"Hanging Lights", "White Jhoomars", "Jhoomars with MP3 Players"};
    String[] productPrice = new String[]{"8000", "7000", "5000"};
    int[] prodcutImgs = new int[]{R.drawable.img_lf, R.drawable.img_lt, R.drawable.img_lth};


    public ProductsFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.product_fragment_layout, container, false);
        ((CategoriesActivity)getContext()).getSupportActionBar().setTitle(AppConstants.SUBCATHEAD);
        setRecyclerView(myFragmentView);
        return myFragmentView;
    }

    public void setRecyclerView(View v) {
        productRV = (RecyclerView) v.findViewById(R.id.productRV);
        productRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        ProductListAdapter pla = new ProductListAdapter(getActivity(), productNamesList, productPrice, prodcutImgs);

        productRV.setAdapter(pla);

    }
}
