package hnweb.com.goluramapp.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import hnweb.com.goluramapp.CategoriesActivity;
import hnweb.com.goluramapp.R;
import hnweb.com.goluramapp.utility.AppConstants;
import hnweb.com.goluramapp.utility.NumberPickerView;

/**
 * Created by neha on 2/7/2017.
 */

public class ProductDetailsFragment extends Fragment implements View.OnClickListener {
    TextView quantityTV, productImgTV, priceTV, addtocartTV;
    public static int quantity = 0;


    public ProductDetailsFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.product_details_fragment, container, false);
        init(myFragmentView);

        return myFragmentView;
    }

    public void init(View v) {
        quantityTV = (TextView) v.findViewById(R.id.quantityTV);
        productImgTV = (TextView) v.findViewById(R.id.productImgTV);
        priceTV = (TextView) v.findViewById(R.id.priceTV);
        addtocartTV = (TextView) v.findViewById(R.id.addtocartTV);
        addtocartTV.setOnClickListener(this);
        quantityTV.setOnClickListener(this);
        setData();
    }

    public void setData() {
        ((CategoriesActivity) getContext()).getSupportActionBar().setTitle(AppConstants.PNAME);
        productImgTV.setBackgroundResource(AppConstants.PIMAGE);
        priceTV.setText(AppConstants.PPRICE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addtocartTV:

                if (!AppConstants.pNameList.contains(AppConstants.PNAME)) {
                    String[] qty = quantityTV.getText().toString().split(" ");
                    AppConstants.pNameList.add(AppConstants.PNAME);
//                    int price = Integer.parseInt(AppConstants.PPRICE) * Integer.parseInt(qty[1]);
                    AppConstants.pPriceList.add(AppConstants.PPRICE);

                    AppConstants.pQuantityList.add(qty[1]);
                    AppConstants.pImgList.add(AppConstants.PIMAGE);
                    TextView itemCountTV = (TextView) getActivity().findViewById(R.id.itemCountTV);
                    if (AppConstants.pNameList.size() < 10) {

                        itemCountTV.setText("0" + AppConstants.pNameList.size());
                    } else {
                        itemCountTV.setText(AppConstants.pNameList.size() + "");
                    }

                    Toast.makeText(getActivity(), "Product added to cart successfully.", Toast.LENGTH_SHORT).show();


                } else {
                    Toast.makeText(getActivity(), "Already added in cart", Toast.LENGTH_SHORT).show();
                }


                Log.e("NAME", AppConstants.pNameList.toString());
                break;
            case R.id.quantityTV:
                editQuantityDialog(getActivity(), quantityTV);
                break;
        }
    }

    public static void editQuantityDialog(final Activity context, final TextView quantityTV) {
        final Dialog settingsDialog = new Dialog(context);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setCancelable(false);
        settingsDialog.setContentView(context.getLayoutInflater().inflate(R.layout.edit_quantity_dialog
                , null));
        settingsDialog.setCancelable(true);

        final NumberPickerView npv = (NumberPickerView) settingsDialog.findViewById(R.id.dialog_edit_newquantity_npv);
        TextView setquantityTV = (TextView) settingsDialog.findViewById(R.id.setquantityTV);
//        npv.setValue(Integer.parseInt(quantityTV.getText().toString()));
        setquantityTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quantity = npv.getValue();
                quantityTV.setText("Quantity: " + quantity);
                settingsDialog.dismiss();
            }
        });

        settingsDialog.show();
    }


}
