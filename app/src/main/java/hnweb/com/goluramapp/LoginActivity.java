package hnweb.com.goluramapp;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import hnweb.com.goluramapp.utility.TelephonyInfo;
import hnweb.com.goluramapp.utility.Validations;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText emailET, passET;
    ImageView registerIV;
    String email, pass;
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "GoluRamAppPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        init();
    }

    public void init() {
        passET = (EditText) findViewById(R.id.passET);
        emailET = (EditText) findViewById(R.id.emailET);
    }

    public void getData() {
        email = emailET.getText().toString().trim();
        pass = passET.getText().toString().trim();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.loginBTN:

//                String[] cellArray = new String[]{"8626090490;", "9527626289"};
//                String toNumbers = "8626090490;" + "9527626289";
//                sendSMS(toNumbers, "Helloooooo");
//                Uri sendSmsTo = Uri.parse("smsto:" + toNumbers);
//                Intent intent1 = new Intent(
//                        android.content.Intent.ACTION_SENDTO, sendSmsTo);
//                intent1.putExtra("sms_body", "Hello");
//                startActivity(intent1);

                checkValid();
                break;
            case R.id.fbBTN:
                Toast.makeText(this, "Coming soon ...", Toast.LENGTH_SHORT).show();
                break;
            case R.id.create_accountBTN:
                Intent intent = new Intent(this, RegisterActivity.class);
                startActivity(intent);
                break;

        }
    }

    public void checkValid() {
        getData();
        if (Validations.strslength(email) && Validations.strslength(pass)) {
            if (Validations.emailCheck(email)) {
                if (Validations.strslength(pass)) {
                    Toast.makeText(this, "Coming soon ...", Toast.LENGTH_SHORT).show();

                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    editor.putString("email", email);
                    editor.commit();


                    Intent intent = new Intent(this, CategoriesActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    passET.setError("Please enter password.");
                    passET.requestFocus();
                }
            } else {
                emailET.setError("Please enter valid email id.");
                emailET.requestFocus();
            }
        } else {
            emailET.setError("Please enter valid email id.");
            emailET.requestFocus();
            passET.setError("Please enter password.");
        }
    }


    private void sendSMS(String phoneNumber, String message) {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS sent",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:


                        dualSim();


                        Toast.makeText(getBaseContext(), "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }


    public void dualSim() {
        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(LoginActivity.this);
        boolean isSIM1Ready = telephonyInfo.isSIM1Ready();
        boolean isSIM2Ready = telephonyInfo.isSIM2Ready();
        boolean isDualSIM = telephonyInfo.isDualSIM();

        Log.e("IS DUAL SIM", "" + isDualSIM);
        Log.e("isSIM1Ready", "" + isSIM1Ready);
        Log.e("isSIM2Ready", "" + isSIM2Ready);

        if (isDualSIM) {
            if (isSIM1Ready || isSIM2Ready) {
                Toast.makeText(this, "An Unexpected failure occured while sending SMS. Please check whether you have working SMS plan and try again later.",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Please activate atleast one SIM for sending SMS and retry.",
                        Toast.LENGTH_LONG).show();
            }
        } else {
            if (!isSIM1Ready) {
                Toast.makeText(this, "No SIM detected to perform this action.",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "An Unexpected failure occured while sending SMS. Please check whether you have working SMS plan and try again later.",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}
