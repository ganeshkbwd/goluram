package hnweb.com.goluramapp;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import hnweb.com.goluramapp.fragments.AccountFragment;
import hnweb.com.goluramapp.fragments.CategoryFragment;
import hnweb.com.goluramapp.fragments.MyCartFragment;
import hnweb.com.goluramapp.utility.AppConstants;

public class CategoriesActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    TextView myCartTV, categoryTV, accountTV, itemCountTV;
    public static boolean doubleBackToExitPressedOnce = false;
    public static final String MyPREFERENCES = "GoluRamAppPrefs";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        setToolbarDrawer();

        setMyfragment();
    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Category");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        settingDrawerLL = (LinearLayout) findViewById(R.id.settings_drawer_layout);

//        myPaymentTV = (TextView) settingDrawerLL.findViewById(R.id.myorderBTN);
//        myPaymentTV.setText("MY ORDER");

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                    Constants.profileWebservice(MyHunts_Activity.this, String.valueOf(user_id), iv, nameTV);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        myCartTV = (TextView) findViewById(R.id.myCartTV);
        categoryTV = (TextView) findViewById(R.id.categoryTV);
        accountTV = (TextView) findViewById(R.id.accountTV);
        itemCountTV = (TextView) findViewById(R.id.itemCountTV);


        if (AppConstants.pNameList.size() < 10) {
            itemCountTV.setText("0" + AppConstants.pNameList.size());
        } else {
            itemCountTV.setText(AppConstants.pNameList.size() + "");
        }


    }

    public void setMyfragment() {
        //add a fragment
        CategoryFragment myFragment = new CategoryFragment();
        replaceFragment(myFragment);
        myCartTV.setTextColor(Color.WHITE);
        categoryTV.setTextColor(Color.RED);
        accountTV.setTextColor(Color.WHITE);
    }

    public void replaceFragment(android.support.v4.app.Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragmentLL, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.addToBackStack(fragment.getClass().getName());
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.myCartRL:
                while(getSupportFragmentManager().popBackStackImmediate()){}
                myCartTV.setTextColor(Color.RED);
                categoryTV.setTextColor(Color.WHITE);
                accountTV.setTextColor(Color.WHITE);
                MyCartFragment myCart = new MyCartFragment();
                replaceFragment(myCart);
                drawerLayout.closeDrawers();
                break;
            case R.id.categoryTV:
                while(getSupportFragmentManager().popBackStackImmediate()){}
                myCartTV.setTextColor(Color.WHITE);
                categoryTV.setTextColor(Color.RED);
                accountTV.setTextColor(Color.WHITE);
                CategoryFragment myFragment = new CategoryFragment();
                replaceFragment(myFragment);
                drawerLayout.closeDrawers();
                break;
            case R.id.accountTV:
                while(getSupportFragmentManager().popBackStackImmediate()){}
                myCartTV.setTextColor(Color.WHITE);
                categoryTV.setTextColor(Color.WHITE);
                accountTV.setTextColor(Color.RED);
                AccountFragment accFragment = new AccountFragment();
                replaceFragment(accFragment);
                drawerLayout.closeDrawers();
                break;
            case R.id.signOutTv:
                logout(this);
                drawerLayout.closeDrawers();
                break;
        }
    }

    public void logout(final Activity context) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context,
                        LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
//                toastDialog(context, "You are logout successfully");
                Toast.makeText(context,
                        "You are logout successfully", Toast.LENGTH_LONG).show();
                SharedPreferences settings = context.getApplicationContext()
                        .getSharedPreferences(MyPREFERENCES,
                                Context.MODE_PRIVATE);
                settings.edit().clear().commit();
                context.finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();


    }

    @Override
    public void onBackPressed() {


        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {

            if (doubleBackToExitPressedOnce){
                this.finish();
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click back again to exit from GoluRam App", Toast.LENGTH_SHORT)
                    .show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

//            Toast.makeText(this,
//
//                    getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName() + "", Toast.LENGTH_LONG).show();

        } else {
                super.onBackPressed();
        }


    }
}
