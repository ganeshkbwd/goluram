package hnweb.com.goluramapp.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import hnweb.com.goluramapp.R;
import hnweb.com.goluramapp.utility.AppConstants;
import hnweb.com.goluramapp.utility.NumberPickerView;

/**
 * Created by neha on 2/7/2017.
 */

public class MyCartAdapter extends RecyclerView.Adapter<MyCartAdapter.MyCartViewHolder> {
    Context context;
    ArrayList<String> pNameList, pQuantityList, pPriceList;
    ArrayList<Integer> pImgList;
    TextView totalpriceTV;
    static int quantity;

    public MyCartAdapter(Activity activity, ArrayList<String> pNameList, ArrayList<Integer> pImgList, ArrayList<String> pQuantityList, ArrayList<String> pPriceList, TextView totalpriceTV) {
        this.context = activity;
        this.pNameList = pNameList;
        this.pImgList = pImgList;
        this.pQuantityList = pQuantityList;
        this.pPriceList = pPriceList;
        this.totalpriceTV = totalpriceTV;

    }

    @Override
    public MyCartAdapter.MyCartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(context).inflate(R.layout.mycart_list_item, null);
        MyCartAdapter.MyCartViewHolder dlvh = new MyCartAdapter.MyCartViewHolder(layoutView);
        return dlvh;
    }

    @Override
    public void onBindViewHolder(final MyCartAdapter.MyCartViewHolder holder, final int position) {

        Log.e("Product", pNameList.toString().trim());
        holder.itemImgTV.setBackgroundResource(AppConstants.prodcutImgs1[position]);
        holder.itemNameTV.setText(pNameList.get(position).toString().trim());
        holder.itemQuantityTV.setText(pQuantityList.get(position).toString().trim());
        int price = Integer.parseInt(pPriceList.get(position).toString().trim()) * Integer.parseInt(pQuantityList.get(position).toString().trim());

        holder.itemPriceTV.setText(String.valueOf(price));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                chooseOptionDialog(position, holder.itemPriceTV);
//                notifyItemRangeChanged(position, pNameList.size(), pImgList.size(), pQuantityList.size(), pPriceList.size());
            }
        });

    }

    @Override
    public int getItemCount() {
        return pNameList.size();
    }

    public class MyCartViewHolder extends RecyclerView.ViewHolder {
        public TextView itemImgTV, itemNameTV, itemPriceTV, itemQuantityTV;

        public MyCartViewHolder(View itemView) {
            super(itemView);
            itemImgTV = (TextView) itemView.findViewById(R.id.itemImgTV);
            itemNameTV = (TextView) itemView.findViewById(R.id.itemNameTV);
            itemPriceTV = (TextView) itemView.findViewById(R.id.itemPriceTV);
            itemQuantityTV = (TextView) itemView.findViewById(R.id.itemQuantityTV);

        }


    }

    public void chooseOptionDialog(final int position, final TextView itemPriceTV) {
        final Dialog settingsDialog = new Dialog(context);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setCancelable(false);
        settingsDialog.setContentView(((Activity) context).getLayoutInflater().inflate(R.layout.choose_option_dialog
                , null));
        settingsDialog.setCancelable(true);

        Button editBTN, deleteBTN, cancelBTN;
        editBTN = (Button) settingsDialog.findViewById(R.id.editBTN);
        deleteBTN = (Button) settingsDialog.findViewById(R.id.deleteBTN);
        cancelBTN = (Button) settingsDialog.findViewById(R.id.cancelBTN);

        editBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int prqty = Integer.parseInt(AppConstants.pQuantityList.get(position).toString().trim());
                editQuantityDialog((Activity) context, prqty, position, itemPriceTV);
                settingsDialog.dismiss();
            }
        });

        deleteBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pNameList.remove(position);
                pQuantityList.remove(position);
                pPriceList.remove(position);
                pImgList.remove(position);
                notifyDataSetChanged();
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, pNameList.size());
                notifyItemRangeChanged(position, pImgList.size());
                notifyItemRangeChanged(position, pQuantityList.size());
                notifyItemRangeChanged(position, pPriceList.size());

                int total = 0;
                for (int i = 0; i < AppConstants.pPriceList.size(); i++) {
                    total = total + (Integer.parseInt(AppConstants.pPriceList.get(i).toString().trim()) * Integer.parseInt(AppConstants.pQuantityList.get(i).toString().trim()));
                }
                AppConstants.TOTAL = total;
                totalpriceTV.setText(String.valueOf(total));

                Toast.makeText(context,"Product Item removed from cart successfully.",Toast.LENGTH_SHORT).show();
                settingsDialog.dismiss();
            }
        });
        cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingsDialog.dismiss();
            }
        });

        settingsDialog.show();
    }


    public void editQuantityDialog(final Activity context, int prqty, final int position, final TextView itemPriceTV) {
        final Dialog settingsDialog = new Dialog(context);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setCancelable(false);
        settingsDialog.setContentView(context.getLayoutInflater().inflate(R.layout.edit_quantity_dialog
                , null));
        settingsDialog.setCancelable(true);

        final NumberPickerView npv = (NumberPickerView) settingsDialog.findViewById(R.id.dialog_edit_newquantity_npv);
        TextView setquantityTV = (TextView) settingsDialog.findViewById(R.id.setquantityTV);
        npv.setValue(prqty);
//        npv.setValue(Integer.parseInt(quantityTV.getText().toString()));
        setquantityTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quantity = npv.getValue();
                AppConstants.pQuantityList.set(position, String.valueOf(quantity));
                int price = Integer.parseInt(pPriceList.get(position).toString().trim()) * Integer.parseInt(pQuantityList.get(position).toString().trim());
//                AppConstants.pPriceList.set(position,)
                itemPriceTV.setText(String.valueOf(price));

                int total = 0;
                for (int i = 0; i < AppConstants.pPriceList.size(); i++) {
                    total = total + (Integer.parseInt(AppConstants.pPriceList.get(i).toString().trim()) * Integer.parseInt(AppConstants.pQuantityList.get(i).toString().trim()));
                }
                AppConstants.TOTAL = total;
                totalpriceTV.setText(String.valueOf(total));
                notifyDataSetChanged();

                Toast.makeText(context,"Product Item quantity updated successfully.",Toast.LENGTH_SHORT).show();

                settingsDialog.dismiss();
            }
        });

        settingsDialog.show();
    }
}
