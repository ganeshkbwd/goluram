package hnweb.com.goluramapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import hnweb.com.goluramapp.R;

/**
 * Created by neha on 2/8/2017.
 */

public class CheckOutAdapter extends RecyclerView.Adapter<CheckOutAdapter.CheckOutViewholder> {
    Context context;
    ArrayList<String> pNameList, pQuantityList, pPriceList;


    public CheckOutAdapter(Activity activity, ArrayList<String> pNameList, ArrayList<String> pQuantityList, ArrayList<String> pPriceList) {
        this.context = activity;
        this.pNameList = pNameList;
        this.pQuantityList = pQuantityList;
        this.pPriceList = pPriceList;

    }

    @Override
    public CheckOutAdapter.CheckOutViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(context).inflate(R.layout.checkout_lit_item, null);
        CheckOutAdapter.CheckOutViewholder dlvh = new CheckOutAdapter.CheckOutViewholder(layoutView);
        return dlvh;
    }

    @Override
    public void onBindViewHolder(CheckOutAdapter.CheckOutViewholder holder, int position) {

        holder.amountTV.setText(pPriceList.get(position).toString().trim());
        holder.itemTV.setText(pNameList.get(position).toString().trim());
        holder.qtyTV.setText(pQuantityList.get(position).toString().trim());


    }

    @Override
    public int getItemCount() {
        return pNameList.size();
    }

    public class CheckOutViewholder extends RecyclerView.ViewHolder {
        public TextView itemTV, qtyTV, amountTV;

        public CheckOutViewholder(View itemView) {
            super(itemView);
            itemTV = (TextView) itemView.findViewById(R.id.itemTV);
            qtyTV = (TextView) itemView.findViewById(R.id.qtyTV);
            amountTV = (TextView) itemView.findViewById(R.id.amountTV);
        }
    }
}
