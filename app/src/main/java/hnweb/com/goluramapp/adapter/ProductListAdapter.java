package hnweb.com.goluramapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hnweb.com.goluramapp.CategoriesActivity;
import hnweb.com.goluramapp.R;
import hnweb.com.goluramapp.fragments.ProductDetailsFragment;
import hnweb.com.goluramapp.utility.AppConstants;

/**
 * Created by neha on 2/6/2017.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductViewHolder> {
    Context context;
    String[] productNamesList;
    String[] productPrice;
    int[] prodcutImgs;

    public ProductListAdapter(Activity activity, String[] productNamesList, String[] productPrice, int[] prodcutImgs) {
        this.context = activity;
        this.productNamesList = productNamesList;
        this.productPrice = productPrice;
        this.prodcutImgs = prodcutImgs;
    }

    @Override
    public ProductListAdapter.ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(context).inflate(R.layout.product_list_item, null);
        ProductViewHolder dlvh = new ProductViewHolder(layoutView);
        return dlvh;
    }

    @Override
    public void onBindViewHolder(final ProductListAdapter.ProductViewHolder holder, final int position) {
        holder.productIV.setBackgroundResource(prodcutImgs[position]);
        holder.nameTV.setText(productNamesList[position].toString().trim());
        holder.priceTV.setText(productPrice[position].toString().trim());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppConstants.PNAME = holder.nameTV.getText().toString().trim();
                AppConstants.PPRICE = holder.priceTV.getText().toString().trim();
                AppConstants.PIMAGE = prodcutImgs[position];
                ProductDetailsFragment pdf = new ProductDetailsFragment();
                ((CategoriesActivity) context).replaceFragment(pdf);
            }
        });

    }

    @Override
    public int getItemCount() {
        return productNamesList.length;
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        public TextView productIV, nameTV, priceTV;

        public ProductViewHolder(View itemView) {
            super(itemView);
            productIV = (TextView) itemView.findViewById(R.id.productIV);
            nameTV = (TextView) itemView.findViewById(R.id.nameTV);
            priceTV = (TextView) itemView.findViewById(R.id.priceTV);

        }

    }
}
