package hnweb.com.goluramapp.utility;

import android.text.TextUtils;
import android.util.Patterns;

/**
 * Created by neha on 12/29/2016.
 */

public class Validations {
    public static boolean strslength(String word) {
        if (word.length() > 0 && !TextUtils.isEmpty(word))
            return true;
        else
            return false;
    }

    public static boolean emailCheck(String email) {
        if (strslength(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches())
            return true;
        else
            return false;
    }

    public static boolean confirmPass(String password, String confirmPass) {
        if (strslength(confirmPass) && password.matches(confirmPass))
            return true;
        else
            return false;
    }
}
