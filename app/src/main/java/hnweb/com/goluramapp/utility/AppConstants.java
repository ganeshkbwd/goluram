package hnweb.com.goluramapp.utility;

import java.util.ArrayList;

import hnweb.com.goluramapp.R;

/**
 * Created by neha on 2/6/2017.
 */

public class AppConstants {

    public static String SUBCATHEAD = "";
    public static String CATHEAD = "";
    public static ArrayList<String> pNameList = new ArrayList<String>();
    public static ArrayList<String> pQuantityList = new ArrayList<String>();
    public static ArrayList<String> pPriceList = new ArrayList<String>();
    public static ArrayList<Integer> pImgList = new ArrayList<Integer>();

    public static String PNAME;
    public static String PQUANTITY;
    public static String PPRICE;
    public static int PIMAGE;
    public static int TOTAL = 0;

    public static int[] prodcutImgs1 = new int[]{R.drawable.img_f, R.drawable.img_t, R.drawable.img_th};
}
